import axiosInstance from "./popup/Services/axiosService";

export function refreshJwt() {
  axiosInstance
    .get("JWTRefresh", {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwtToken")
      }
    })
    .then(res => {
      localStorage.setItem("jwtToken", res.data);
      console.log("Refreshed");
    })
    .catch(err => {
      if (err.response.status === 413) {
        localStorage.clear();
      }
      console.log(err);
    });
}

export function JwtExpired() {
  axiosInstance
    .get("JwtExpired", {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwtToken")
      }
    })
    .then()
    .catch(err => {
      if (err.response.status === 401) {
        console.log(err.response);
        refreshJwt();
        console.log("jwtRefreshed");
      }
      if (err.response.status === 500) {
        console.log(err.response);
      }
    });
}
