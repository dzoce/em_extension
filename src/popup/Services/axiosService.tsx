import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://em.boo/api",
  headers: { Accept: "application/json", "Content-Type": "application/json" }
});

export default axiosInstance;
