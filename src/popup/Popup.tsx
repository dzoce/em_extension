import * as React from "react";
import "./Popup.scss";
import Login from "./Login/Login";
import Header from "./Header/Header";
import Tabs from "./Main/Tabs/Tabs";
import Settings from "./Header/Settings/Settings";
import Mail from "./Header/Mail/Mail";
import Help from "./Header/Help/Help";
import Deals from "./Main/Deals/Deals";
import Referrals from "./Main/Referrals/Referrals";
import { refreshJwt, JwtExpired } from "../helpers";

interface AppProps {}

interface AppState {
  loggedIn: boolean;
  headerLink: string;
  jwtToken: string;
}

export default class Popup extends React.Component<AppProps, AppState> {
  constructor(props: AppProps, state: AppState) {
    super(props, state);
    this.state = { loggedIn: false, headerLink: null, jwtToken: null };
  }

  componentWillMount() {
    JwtExpired();
    if (JSON.parse(localStorage.getItem("user")) === null)
      this.setState({ loggedIn: false, headerLink: "loggin" });
  }
  componentDidMount() {
    // Example of how to send a message to eventPage.ts.

    if (JSON.parse(localStorage.getItem("user")) === null)
      this.setState({ headerLink: "login", loggedIn: false });
    else
      this.setState({
        loggedIn: true,
        headerLink: "deals",
        jwtToken: localStorage.getItem("jwtToken")
      });

    chrome.runtime.sendMessage({ popupMounted: true });
  }

  handleClick = headerLink => {
    console.log(headerLink);
    this.setState({
      headerLink
    });
  };

  loginCallback = resData => {
    localStorage.setItem("jwtToken", resData.token);
    localStorage.setItem("user", JSON.stringify(resData.user));
    localStorage.setItem("loggedIn", JSON.stringify(true));

    this.setState({ headerLink: "deals", loggedIn: true });
  };
  renderSwitch(headerLink) {
    switch (headerLink) {
      case "settings":
        return <Settings changePage={this.handleClick} />;
      case "mail":
        if (this.state.loggedIn) {
          return <Mail changePage={this.handleClick} />;
        }
      case "help":
        return <Help changePage={this.handleClick} />;
      case "login":
        return (
          <Login
            loggedIn={this.state.loggedIn}
            loginCallback={this.loginCallback}
          />
        );
      case "deals":
        if (this.state.loggedIn) {
          return <Deals />;
        }
      case "referrals":
        return <Referrals />;
    }
  }

  render() {
    return (
      <div className="popupContainer">
        <Header changePage={this.handleClick} loggedIn={this.state.loggedIn} />
        {this.state.loggedIn == true ? (
          <Tabs changePage={this.handleClick} />
        ) : (
          ""
        )}

        {this.renderSwitch(this.state.headerLink)}
      </div>
    );
  }
}
