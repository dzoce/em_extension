import * as React from "react";

export interface TabsProps {
  changePage:any;
}

export interface TabsState {}

class Tabs extends React.Component<TabsProps, TabsState> {

  changeClass=(e)=>{
    var active=document.querySelector(".active");
    active.classList.remove("active");
    e.target.className="active";
  }
  render() {
    return (
      <div className="tabs">
        <a className="active" onClick={(e)=>{this.props.changePage('deals'); this.changeClass(e)} }>My Deals</a>
        <a  onClick={(e)=>{this.props.changePage('referrals'); this.changeClass(e)}}>Get $25</a>
      </div>
    );
  }
}

export default Tabs;
