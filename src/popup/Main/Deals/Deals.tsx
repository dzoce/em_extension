import * as React from "react";
import axiosInstance from "../../Services/axiosService";
import { DebounceInput } from "react-debounce-input";
// import Explore from "./../Explore/Explore";

export interface DealsProps {}

export interface DealsState {
  deals: any;
  query: string;
  suggestions: any;
}

class Deals extends React.Component<DealsProps, DealsState> {
  constructor(props: DealsProps, state: DealsState) {
    super(props, state);

    this.state = { deals: null, query: "", suggestions: null };
  }

  componentWillMount() {
    this.setState({ deals: JSON.parse(localStorage.getItem("deals")) });
  }

  handleClick = el =>{
    let data={
      "deal":el as Object,
      "user":JSON.parse(localStorage.getItem('user')) as Object

    }
    axiosInstance.post("/dealCallback",data,{
      headers:{
        Authorization: "Bearer " + localStorage.getItem("jwtToken")
      }
    }).then(res=>{
      window.open(el.trackingLink,"_blank");
      console.log(res.data);
    })

  }
  handleInputchange = e => {
    this.setState({ query: e.target.value });
    e.persist();
    console.log("API");
    axiosInstance
      .get(`dealSearch/${e.target.value}`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("jwtToken")
        }
      })
      .then(res => {
        this.state.query.length == 0
          ? this.setState({ suggestions: null })
          : this.setState({ suggestions: res.data });
      })
      .catch(err => {
        console.log(err.response);
      });

    if (e.target.value.length == 0) {
      this.setState({ suggestions: null });
    }
  };

  render() {
    return (
      <div>
        <div className="main_banner">
          <p>
            We wish you happy holidays with <br /> <span>10% Cash Back</span>
          </p>
        </div>
        <div className="search">
          <DebounceInput
            debounceTimeout={500}
            placeholder="Search for Deals or Stores"
            onChange={e => this.handleInputchange(e)}
          />
        </div>
        {this.state.suggestions != null &&
          this.state.suggestions.length > 0 && (
            <h2 className="primary_header"> Search Results </h2>
          )}
        {this.state.suggestions != null &&
          this.state.suggestions.length == 0 && (
            <h2 className="primary_header">No Matches For That Keyword</h2>
          )}
        {this.state.suggestions != null
          ? this.state.suggestions.map((el, i) => {
            console.log(el);
              return (
                <div key={i} className="deal" onClick={()=>this.handleClick(el)}>
                  {el.name}
                  {/* {React.createElement("div", { dangerouslySetInnerHTML: { __html: el.em_button } })} */}


                </div>
              );
            })
          : ""}

        <div className="deals" >
          {this.state.deals != null && this.state.deals.length > 0 && (
            <h2 className="primary_header"> Deals from This Page</h2>
          )}
          {this.state.deals != null &&
            this.state.deals.map((el, i) => {
              console.log(el);
              return (
                <div key={i} className="deal" onClick={()=>this.handleClick(el)}>
                  {el.name}
                  {React.createElement("div", { dangerouslySetInnerHTML: { __html: el.em_button } })}
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default Deals;
