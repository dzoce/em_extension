import * as React from "react";
import axiosInstance from "../../Services/axiosService";
import {FacebookIcon,TwitterIcon, FacebookShareButton, TwitterShareButton} from "react-share";
export interface ReferralsProps {}

export interface ReferralsState {
  share:any;
  emails: any;
  text: string;
}

class Referrals extends React.Component<ReferralsProps, ReferralsState> {
  constructor(props: ReferralsProps, state: ReferralsState) {
    super(props, state);
    this.state = { emails: Array(), text: "",share:null};
  }

  componentWillMount(){
    let userObj=JSON.parse(localStorage.getItem('user'));
    axiosInstance.get(`share/${userObj.uid}`,{
      headers:{ Authorization: "Bearer " + localStorage.getItem("jwtToken")}
    }).then(res=>{
      this.setState({share:res.data});
      console.log(res);
    });
  }

  handleKeyPress = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(e.target.value)){
      var updatedArray = this.state.emails;
      updatedArray.push(e.target.value);
      this.setState({ emails: updatedArray, text: "" });
      }
      console.log(this.state.emails);
    }
  };

  handleOnChange = e => {
    this.setState({ text: e.target.value });
  };

  handleOnClick = e => {
    console.log(this.state.emails.indexOf(this.state.text));
    if(this.state.text!=="" && this.state.emails.indexOf(this.state.text)==-1)
    {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(this.state.text)){
      var updatedArray = this.state.emails;
      updatedArray.push(this.state.text);
      this.setState({ emails: updatedArray, text: "" });
      }
    }
    let data = {
      emails: this.state.emails,
      user: JSON.parse(localStorage.getItem("user")) as Object
    };
    axiosInstance
      .post("/user/inviteFriends", data, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("jwtToken")
        }
      })
      .then(res => {
        console.log(res);
        this.setState({emails:null});
      });
  };
  copyLink = e => {
    const textField = document.createElement("textarea");
    textField.innerText = "url";
    document.body.appendChild(textField);
    textField.select();
    document.execCommand("copy");
    textField.remove();
  };
  render() {
    console.log(this.state.emails);
    return (
      <div>
        <div className="inviteMail">
          <h2>Invite By E-mail</h2>
          <input
            onKeyPress={this.handleKeyPress}
            onChange={this.handleOnChange}
            value={this.state.text}
            placeholder="E-mail to invite"
          />
          <div>
            {this.state.emails != null &&
              this.state.emails.map(e => {
                return <div className="inviteMail-children">{e}</div>;
              })}
          </div>

          <button onClick={this.handleOnClick}>Invite</button>
        </div>
        <div className="referral_section_separator">OR</div>
        <div className="sendLink">
          <h2>Invite By Link</h2>
          <input readOnly value={`https://www.earnmakers.com/r/${JSON.parse(localStorage.getItem('user')).uid}`}/>
          <button onClick={this.copyLink}>Copy Link</button>
          <div style={{display:"flex"}}>
            <div style={{margin:3}}>
              <FacebookShareButton children={<FacebookIcon round={true} size={32}/>} url={"https://www.test.com"}/>
            </div>
            <div style={{margin:3}}>
            <TwitterShareButton children={<TwitterIcon round={true} size={32}/>} url={"https://.test.com"}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Referrals;
