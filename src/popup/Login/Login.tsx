import * as React from "react";
// import postRequest from "../Services/";
import axiosInstance from "../Services/axiosService";
export interface LoginProps {
  loggedIn: boolean;
  loginCallback: any;
}

export interface LoginState {
  email: string;
  password: string;
}

class Login extends React.Component<LoginProps, LoginState> {
  constructor(props: LoginProps, state: LoginState) {
    super(props, state);

    this.state = { email: null, password: null };
  }
  componentDidMount() {
    console.log(this.props);
  }

  login = () => {
    axiosInstance
      .post(
        "/user/login",
        JSON.stringify({
          email: this.state.email,
          password: this.state.password
        })
      )
      .then(res => {
        console.log(res);
        if (res.status === 200) {
          this.props.loginCallback(res.data);
        }
      });
  };
  enterLogin = e => {
    e.key == "Enter" ? this.login() : "";
  };

  render() {
    return (
      <div>
        <div className="logo_text">
          <img src={"../images/logo.png"} />
          <p>Welcome to Your Browser Extension!</p>
        </div>
        <div className="login_input">
          <input
            type="text"
            name="email"
            id="email"
            placeholder="Email"
            onKeyPress={e => this.enterLogin(e)}
            onChange={event => this.setState({ email: event.target.value })}
          />
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Password"
            onKeyPress={e => this.enterLogin(e)}
            onChange={event => this.setState({ password: event.target.value })}
          />
          <button type="submit" onClick={this.login}>
            Sign In
          </button>
        </div>
      </div>
    );
  }
}

export default Login;
