import * as React from "react";

export interface MailProps {
  changePage: any;
}

export interface MailState {}

class Mail extends React.Component<MailProps, MailState> {
  changePage = link => {
    this.props.changePage(link);
  };
  render() {
    return (
      <div>
        <h2>This is a mail page</h2>
        <a onClick={() => this.changePage("deals")}>Back</a>
      </div>
    );
  }
}

export default Mail;
