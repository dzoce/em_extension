import * as React from "react";
import axiosInstance from "../Services/axiosService";

export interface HeaderProps {
  changePage: any;
  loggedIn: boolean;
}

export interface HeaderState {
  clickedLink: "string";
  totalEarnings:String;
  pending:String;
}

class Header extends React.Component<HeaderProps, HeaderState> {
  constructor(props) {
    super(props);
    this.state = {
      clickedLink: null,
      totalEarnings:null,
      pending:null,
    };
  }
  componentWillMount(){
    axiosInstance.get(`user/${JSON.parse(localStorage.getItem('user')).id}/totalEarnings`,{
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwtToken")
      }
    }).then(res=>{
        this.setState({totalEarnings:res.data});
        localStorage.setItem('totalEarnings',res.data);

    
    });

    axiosInstance.get(`user/${JSON.parse(localStorage.getItem('user')).id}/pending`,{
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwtToken")
      }
    }).then(res=>{
            this.setState({pending:res.data});
            localStorage.setItem('pending',res.data);
  
        });
  }
  changePage = link => {
    this.setState({
      clickedLink: link
    });
    this.props.changePage(link);
  };
  render() {
    console.log('render',this.state);
    return (
      <div className="app_header">
        <a className="header_icon" onClick={() => this.changePage("deals")}>
          Home
        </a>
        <div>
          {localStorage.getItem('pending')}
          {localStorage.getItem('totalEarnings')}
        </div>
        {this.props.loggedIn ? (
          <a className="header_icon" onClick={() => this.changePage("mail")}>
            <img src={"../images/icons/mail.png"} />
          </a>
        ) : (
          ""
        )}
        <a className="header_icon" onClick={() => this.changePage("help")}>
          <img src={"../images/icons/help.png"} />
        </a>
        <a className="header_icon" onClick={() => this.changePage("settings")}>
          <img src={"../images/icons/settings.png"} />
        </a>
      </div>
    );
  }
}

export default Header;
