import * as React from "react";

export interface HelpProps {
  changePage: any;
}

export interface HelpState {}

class Help extends React.Component<HelpProps, HelpState> {
  changePage = link => {
    this.props.changePage(link);
  };
  render() {
    return (
      <div>
        <h2>This is a help page!</h2>
        <a onClick={() => this.changePage("deals")}>Back</a>
      </div>
    );
  }
}

export default Help;
