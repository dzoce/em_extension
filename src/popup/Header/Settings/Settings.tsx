import * as React from "react";

export interface SettingsProps {
  changePage: any;
}

export interface SettingsState {}

class Settings extends React.Component<SettingsProps, SettingsState> {
  changePage = link => {
    this.props.changePage(link);
  };
  render() {
    return (
      <div>
        <h2>This is a Settings page!</h2>
        <a onClick={() => this.changePage("deals")}>Back</a>
      </div>
    );
  }
}

export default Settings;
