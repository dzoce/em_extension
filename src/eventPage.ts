// Listen to messages sent from other parts of the extension.
import axiosInstance from "./popup/Services/axiosService";
let urls = require("./data/networksURLs.json");
let previousNotif = "";

// Deals Query Functions
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  // onMessage must return "true" if response is async.
  console.log(request);
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
  chrome.tabs.query({ active: true, lastFocusedWindow: true }, function(tabs) {
    let regex = /:\/\/(.[^/]+)/;
    var url = tabs[0].url.match(regex)[1];
    getDeals(url);
  });
});

chrome.tabs.onUpdated.addListener(function(activeInfo) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    let regex = /:\/\/(.[^/]+)/;
    var url = tabs[0].url.match(regex)[1];
    getDeals(url);
  });
});

function getDeals(url: string) {
  let localStorageUrls=JSON.parse(localStorage.getItem('approvedAdvertisers'));//console.log('localStorageAdv',JSON.parse(localStorage.getItem('approvedAdvertisers')));

  if (localStorageUrls.indexOf(url) > -1) {
    var URL = url.replace("/", "$");

    axiosInstance
      .get(`user/${URL}/deals`, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("jwtToken")
        }
      })
      .then(res => {
        localStorage.removeItem("deals");
        localStorage.setItem("deals", JSON.stringify(res.data));
        if (res.status == 401) localStorage.clear();
        if (res.data.length > 0 && previousNotif != URL) {
          basicNotification(URL);
        }
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    localStorage.removeItem("deals");
  }
}

chrome.notifications.onClicked.addListener(function(notifUrl) {
  chrome.tabs.create({ url: "https://" + notifUrl });
});

function basicNotification(url: string) {
  previousNotif = url;
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { notification: url }, function(
      response
    ) {
    
      });
  });
  var opt = {
    iconUrl: "icon128.png",
    type: "basic",
    title: "Earnmakers",
    message: "This website has a deal for you",
    priority: 1

    
  };
  chrome.notifications.create(url, opt, function() {
    chrome.notifications.getAll(function(ob) {
      console.log({ notifications: ob });
    });
  });
}

chrome.alarms.create('getAdvertisers',
{
  when:1500,
});

chrome.alarms.onAlarm.addListener(()=>{
  axiosInstance.get('getApprovedAdvertisers',{
                headers: {
                  Authorization: "Bearer " + localStorage.getItem("jwtToken")
                }
              }).then(res=>{
          localStorage.setItem('approvedAdvertisers',JSON.stringify(res.data));
          console.log('advData',res.data);
      }).catch(err=>{
        console.log(err);
      })
})